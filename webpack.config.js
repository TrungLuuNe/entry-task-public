module.exports = {
  entry: [
    './src/index.js'
  ],
  output: {
    filename: './bundle.js',
    publicPath: '/'
  },
  devServer: {
    historyApiFallback: true,
  },
  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js', '.css']
  },
  module: {
    loaders: [
      {
        exclude: /node_modules/,
        loader: 'babel'
      }, {
        test: /\.css$/,  
        loader: 'style-loader!css-loader!'
      }
    ]
  }
};