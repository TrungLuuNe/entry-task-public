import { createStore, applyMiddleware, combineReducers } from 'redux';
import logger from 'redux-logger';
import promise from 'redux-promise-middleware';
import {persistStore, persistReducer} from 'redux-persist';
import userLogin from './pages/LoginPage/reducer';
import postContentReducer from './pages/PostContentPage/reducer';
import postReducer from './pages/PostPage/reducer';
import thunkMiddleware from 'redux-thunk';

const rootReducer = combineReducers({
    userLogin,
    postReducer,
    postContentReducer,
});
const store = createStore(
    combineReducers({
        userLogin,
        postReducer,
        postContentReducer,
    }),
    applyMiddleware(
        thunkMiddleware,
        logger(),
        promise(),
    )
);

// const persistedReducer = persistReducer(persistConfig, rootReducer)
// export default () => {
//     let store = createStore(persistedReducer)
//     let persistor = persistStore(store)
//     return { store, persistor }
//   }
export default store;
