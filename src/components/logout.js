import React from 'react';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { logout } from '../pages/LoginPage/action'
import './components.css'

class LogOut extends React.Component {
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }
    onClick() {
        const { logout } = this.props;
        logout();
        this.props.router.history.push('/login');
    }
    render() {
        const { logout } = this.props;
        return (
                <span className="right-icon logout" onClick={this.onClick} ><img src="https://www.iconsdb.com/icons/preview/black/logout-xxl.png" /> </span>
        )
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
      logout: () => {
        dispatch(logout())
      },
     }
  }
  
  
  const LogOutContainer = connect(null,
    mapDispatchToProps,
  )(LogOut)
export default withRouter(LogOutContainer);

