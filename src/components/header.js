import React from "react";
//import '../pages/PostPage/posts.css';
class Header extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div
        className={this.props.openSearch ? "header" : "header header-center"}
      >
        <span>{this.props.children}</span>
      </div>
    );
  }
}

export default Header;
