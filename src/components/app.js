import React from "react";
import { BrowserRouter, Route } from "react-router-dom";
import PrivateRoute from "./private_route";
import Posts from "../pages/PostPage/posts";
import PostContent from "../pages/PostContentPage/postContent";
import Login from "../pages/LoginPage/login";
const App = props => (
  <div>
    <BrowserRouter>
      <div>
        <PrivateRoute exact path="/" component={Posts} />
        <Route exact path="/login" component={Login} />
        <PrivateRoute exact path="/posts" component={Posts} />
        <PrivateRoute exact path="/post" component={PostContent} />
      </div>
    </BrowserRouter>
  </div>
);

export default App;
