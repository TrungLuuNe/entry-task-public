import React from "react";
import { connect } from "react-redux";
import { login } from "./action";
import Loading from "../../components/loading";
import "./login.css";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      submitted: false
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  componentWillMount() {
    const { state, history } = this.props;
    if (state.loggedIn) {
      history.push("/posts");
    }
  }
  componentWillReceiveProps(nextProps) {
    const { state, history } = nextProps;
    if (state.loggedIn) {
      history.push("/posts");
    }
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }
  handleSubmit() {
    const { login } = this.props;
    const { username, password } = this.state;
    this.setState({ submitted: true });
    if (username != "" && password != "") login(username, password);
  }
  getGreeting() {
    const { state } = this.props;
    if (state.loading) {
      return <Loading />;
    }
    if (!state.loggedIn && !this.state.submitted) {
      return <h4>PLEASE LOGIN</h4>;
    } else {
      return <h4>TRY AGAIN</h4>;
    }
  }

  render() {
    const { username, password } = this.state;
    return (
      <div className="container">
        <div className="content">
          <div className="header-section">
            {this.getGreeting()}
            <img src="https://www.iconsdb.com/icons/preview/guacamole-green/arrow-246-xxl.png" />
          </div>
          <form name="form" className="content-section">
            <div className="login-section">
              <div className="input-section">
                <span>
                  <i className="fa fa-user" />
                </span>
                <input
                  placeholder="Username"
                  name="username"
                  value={username}
                  onChange={this.handleChange}
                />
              </div>
              <div className="input-section">
                <span>
                  <i className="fa fa-key" />
                </span>
                <input
                  type="password"
                  placeholder="Password"
                  name="password"
                  value={password}
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="footer-section">
              <input
                type="button"
                value="SIGN IN"
                onClick={this.handleSubmit}
              />
            </div>
          </form>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  state: state.userLogin
});

const mapDispatchToProps = dispatch => {
  return {
    login: (username, password) => {
      dispatch(login(username, password));
    }
  };
};

const LoginContainer = connect(mapStateToProps, mapDispatchToProps)(Login);

export default LoginContainer;
