let user = JSON.parse(localStorage.getItem("user"));
const initalState = user
  ? { loggedIn: true, loading: false, error: null }
  : { loggedIn: false, loading: false, error: null };

function loginReducer(state = initalState, action) {
  let user;
  switch (action.type) {
    case "USER_LOGIN_PENDING":
      return { loggedIn: false, loading: true };
    case "USER_LOGIN_FULFILLED":
      if (action.payload.data.length > 0) {
        user = JSON.stringify(action.payload.data[0]);
        localStorage.setItem("user", user);
        return { loggedIn: true, loading: false };
      } else {
        return { loggedIn: false, loading: false };
      }
    case "USER_LOGIN_REJECTED":
      return {
        loggedIn: false,
        loading: false,
        error: `${action.payload.message}`
      };
    case "USER_LOGOUT":
      localStorage.removeItem("user");
      return { loggedIn: false, loading: false };
    default:
      return state;
  }
}

export default loginReducer;
