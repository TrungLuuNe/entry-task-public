const initalState = {
    comments: [],
    going: [],
    likes: [],
    loading: false,
    error: null,
};

function postContentReducer(state = initalState, action) {
    let likes, going, comments;
    switch (action.type) {
        case 'FETCH_COMMENTS_PENDING':
            return { ...state, loading: true };
        case 'FETCH_COMMENTS_FULFILLED':
            comments = action.payload.data;
            return { ...state, loading: false, comments };
        case 'FETCH_COMMENTS_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        case 'ADD_COMMENT_PENDING':
            return { ...state, loading: true };
        case 'ADD_COMMENT_FULFILLED':
            return { ...state, loading: false};
        case 'ADD_COMMENT_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        case 'FETCH_LIKES_PENDING':
            return { ...state, loading: true, likes: [] };
        case 'FETCH_LIKES_FULFILLED':
            likes = action.payload.data;
            return { ...state, loading: false, likes };
        case 'FETCH_LIKES_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        case 'ADD_LIKES_PENDING':
            // return { ...state, addLoading: true };
            return { ...state, loading: true};
        case 'ADD_LIKES_FULFILLED':
            // return { ...state, addLoading: false };
            return { ...state, loading: false };
        case 'ADD_LIKES_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        case 'FETCH_GOING_PENDING':
            return { ...state, loading: true, going: [] };
        case 'FETCH_GOING_FULFILLED':
            going = action.payload.data;
            return { ...state, loading: false, going };
        case 'FETCH_GOING_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        case 'ADD_GOING_PENDING':
            // return { ...state, addLoading: true };
            return { ...state, loading: true };
        case 'ADD_GOING_FULFILLED':
            // return { ...state, addLoading: false };
            return { ...state, loading: false };
        case 'ADD_GOING_REJECTED':
            return { ...state, loading: false, error: `${action.payload.message}` };
        default:
            return state;
    }
}
export default postContentReducer;