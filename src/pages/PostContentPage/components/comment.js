import React from 'react';

import './comment.css';

const Comment = (props) => (
    <div className="item-comment">
        <div className="item-comment-left-col">
            <img src={props.comment.picture.thumbnail} />
        </div>
        <div className="item-comment-right-col">
            <div className="item-comment-header">
                <span className="item-comment-name">{props.comment.username}</span>
                <span className="item-comment-time">9 hours ago</span>
            </div>
            <div className="item-comment-content">
                {props.comment.body}
            </div>
        </div>
        <span className="item-back-button"><i className="fa fa-arrow-left"></i></span>
    </div>
)
export default Comment;