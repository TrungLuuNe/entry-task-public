import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = (props) => (
  <div className="header">
    <span>
      <Link
        className=''
        onClick={props.onClick}
        to={`/`}
      >
        <span className="left-icon"><img src="http://freepngimg.com/download/home/1-2-home-png-image.png" /></span>
      </Link>
      <span className="right-icon"><img src="http://www.freepngimg.com/download/superman_logo/10-2-superman-logo-png-hd.png" /></span>
    </span>
  </div>)

export default NavBar;

