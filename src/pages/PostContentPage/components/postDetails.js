import React from 'react'
import Slider from 'react-slick';
import '../postContent.css';
import GoingInfo from './goingInfo';
import LikeInfo from './likeInfo';
const PostDetails = (props) => {
    var settings = {
        dots: false,
        arrows: false,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        variableWidth: true
    };
    return (
        <div>
            <div className="item-content item-content-section">
                <Slider {...settings}>
                    <div><img src="http://webneel.com/daily/sites/default/files/images/daily/05-2014/12-sunrise-picture.jpg" /></div>
                    <div><img src="https://image.freepik.com/free-photo/cute-cat-picture_1122-449.jpg" /></div>
                </Slider>
                <p className="item-content-text">
                    {props.postContent.content}
                </p>
                <div className="item-content-viewall-section">
                    <button className="item-content-viewall">VIEW ALL</button>
                </div>
            </div>

            <div className="item-when-section">
                <div className="item-when-section-title">When</div>
                <div className="item-when-section-row">
                    <div className="item-when-section-column">
                        <div className="item-when-section-date">
                            <span><i className="fa fa-angle-double-left"></i></span>
                            <span className="">15 April 2015</span>
                        </div>
                        <div className="item-when-section-time">
                            <span>8:30</span>
                            <span>am</span>
                        </div>
                    </div>
                    <div className="item-when-section-column">
                        <div className="item-when-section-date">
                            <span><i className="fa fa-angle-double-right"></i></span>
                            <span className="">15 April 2015</span>
                        </div>
                        <div className="item-when-section-time">
                            <span>8:30</span>
                            <span>am</span>
                        </div>
                    </div>
                </div>
            </div>

            <div className="item-when-section item-where-section">
                <div className="item-when-section-title">Where</div>
                <div className="item-where-name">Marina Bay Sands</div>
                <div className="item-where-address">10 Bayfront Ave, S018956</div>
                <div className="item-where-map">
                    <img src="https://www.google.com/permissions/images/maps-att.png" />
                </div>
            </div>

            <GoingInfo data={props.going} limit={6}></GoingInfo>
            <LikeInfo data={props.likes}></LikeInfo>
        </div>
    );
}

export default PostDetails;