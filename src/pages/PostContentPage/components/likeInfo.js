import React from 'react';

const LikeInfo = (props) => (
    <div className="going-info-section">
        <div className="going">
            <span><i className="item-content-check-white item-content-icon fa fa-check"></i></span>
            <span className="item-content-check-text item-content-footer-text-normal">{props.data.length} likes</span>
        </div>
        <div className="picture-list">
            {props.data.map((like, i) => {
                if (i < 6)
                    return <span key={i} className="item-small-ava"><img src={like.picture.large} /></span>;
            })}
        </div>
    </div>
)

export default LikeInfo;