import React from 'react';

const GoingInfo = (props) => (
    <div className="going-info-section">
        <div className="going">
            <span><i className="item-content-check-white item-content-icon fa fa-check"></i></span>
            <span className="item-content-check-text item-content-footer-text-normal">{props.data.length} going</span>
        </div>
        <div className="picture-list">
            {props.data.map((going, i) => {
                if (props.limit) {
                    if (i < props.limit)
                        return <span key={i} className="item-small-ava"><img src={going.picture.large} /></span>;
                }
                else {
                    return <span key={i} className="item-small-ava"><img src={going.picture.large} /></span>;
                }
            })}
        </div>
    </div>
)

export default GoingInfo;