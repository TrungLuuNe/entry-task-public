import axios from 'axios'

export function getComments(post_id) {
    return {
      type: 'FETCH_COMMENTS',
      payload: axios.get('http://localhost:3000/comments?postId='+post_id)
    };
  }

export function postCommentReload(comment, postId) {
  let currentUser =JSON.parse(localStorage.getItem('user'));
  let data = JSON.stringify({
    username: currentUser.username,
    body: comment,
    postId: postId,
    picture: currentUser.picture,
  })
  return dispatch => {

    return dispatch({
      type: 'ADD_COMMENT',
      payload: axios.post('http://localhost:3000/comments',data,{
        headers: {
            'Content-Type': 'application/json',
        }
      })}).then(() => dispatch(getComments(postId)))
  };
    
}

export function getGoing(post_id) {
    return {
      type: 'FETCH_GOING',
      payload: axios.get('http://localhost:3000/going?postId='+post_id)
    };
  }


export function getLikes(post_id) {
    return {
      type: 'FETCH_LIKES',
      payload: axios.get('http://localhost:3000/likes?postId='+post_id)
    };
  }


export function addLikeReload(postId) {
  let currentUser =JSON.parse(localStorage.getItem('user'));
  let data = JSON.stringify({
    username: currentUser.username,
    postId: postId,
    userId: currentUser.id,
    picture: currentUser.picture,
  })
  return dispatch => {

    return dispatch({
      type: 'ADD_LIKES',
      payload: axios.post('http://localhost:3000/likes',data,{
        headers: {
            'Content-Type': 'application/json',
        }
      })}).then(() => dispatch(getLikes(postId)))
  };    
}

export function postGoingReload(postId) {
  let currentUser =JSON.parse(localStorage.getItem('user'));
  let data = JSON.stringify({
    username: currentUser.username,
    postId: postId,
    userId: currentUser.id,
    picture: currentUser.picture,
  })
  return dispatch => {

    return dispatch({
      type: 'ADD_GOING',
      payload: axios.post('http://localhost:3000/going',data,{
        headers: {
            'Content-Type': 'application/json',
        }
      })}).then(() => dispatch(getGoing(postId)))
  };    
}


