import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import NavBar from './components/navBar';
import Header from '../../components/header';
import LogOut from '../../components/logout';
import Comment from './components/comment';
import PostDetails from './components/postDetails';
import GoingInfo from './components/goingInfo';
import LikeInfo from './components/likeInfo';
import { getComments, postGoingReload, getGoingReload, addLikeReload, getLikes, postCommentReload } from './action';
import './postContent.css';

class PostContent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      details: 'active',
      comments: '',
      new_comment: '',
      going: false,
      participants: '',
      like: false,
    };
    this.handleChange = this.handleChange.bind(this);
    this.postComment = this.postComment.bind(this);
    this.handleChosenTab = this.handleChosenTab.bind(this);
  }


  componentWillReceiveProps(nextProps) {
    const { createdComment, getComments, postContent, data, getGoing, getLikes } = nextProps;
    let currentUser = JSON.parse(localStorage.getItem('user'));
    for (var value of data.going) {
      if (value.userId == currentUser.id) {
        this.setState({ going: true });
        break;
      }
    }
    for (var value of data.likes) {
      if (value.userId == currentUser.id) {
        this.setState({ like: true });
        break;
      }
    }
  }

  handleChosenTab(value) {
    this.setState({ details: '', comments: '', participants: '' })
    this.setState({ [value]: 'active' });
  }

  handleChange(e) {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  }

  postComment() {
    const { postComment, postContent } = this.props;
    if (this.state.new_comment) {
      postComment(this.state.new_comment, postContent.id);
      this.setState({ new_comment: '' });
    }
  }

  render() {
    const { postContent, createdComment, data, addLike, postComment, addGoing } = this.props;
    return (
      postContent ?
        <div className="container">
          <Header>
            <Link
            className=''
            onClick={this.props.onClick}
            to={`/`}
            >
            <span className="left-icon"><img src="http://freepngimg.com/download/home/1-2-home-png-image.png" /></span>
            </Link>
            <LogOut/>
          </Header>
          <div id="users" className="user-profile">

            <div className="item">
              {this.state.details ?
                (<div>
                  <div className="item-header">
                    <span className="item-header-channel left-icon">{postContent.category}</span>
                  </div>

                  <div className="item-content item-title-section">
                    <p className="item-content-title">Activity Title Name Make it Longer May Longer than One Line</p>
                  </div>

                  <div className="item-name-section">
                    <div><img src={postContent.picture.large} /></div>
                    <div>
                      <p className="item-username">{postContent.login.username}</p>
                      <p className="item-publish">Published 2 days ago</p>
                    </div>
                  </div> </div>) :
                ''}

              <div className="item-button-section">
                <div className="item-button-section-row">
                  <div className={this.state.details} name="details" onClick={this.handleChosenTab.bind(this, "details")} >
                    <span><i className="fa fa-info-circle"></i></span>
                    <span className="">Details</span>
                  </div>
                  <div className={this.state.participants} name="participants" onClick={this.handleChosenTab.bind(this, "participants")}>
                    <span><i className="fa fa-users"></i></span>
                    <span className="">Participants</span>
                  </div>
                  <div className={this.state.comments} name="comments" onClick={this.handleChosenTab.bind(this, "comments")}>
                    <span><i className="fa fa-comments"></i></span>
                    <span className="">Comments</span>
                  </div>
                </div>
              </div>
              {
                this.state.details ?
                  <PostDetails key='details' postContent={postContent} going={data.going} likes={data.likes}></PostDetails> :
                  ''
              }
              {
                this.state.participants ?
                  <div>
                    <GoingInfo data={data.going}></GoingInfo>
                    <LikeInfo data={data.likes}></LikeInfo>
                  </div>
                  : ''
              }
              <div id="comments">
                {data.comments.map((comment, i) => {
                  return <Comment
                    key={i}
                    comment={comment}
                  />
                })}
              </div>
              {
                this.state.details ?
                  <div className="item-footer-button-section">
                    <div className="item-footer-button-col" onClick={this.handleChosenTab.bind(this, "comments")}>
                      <span><i className="fa fa-comment"></i></span>
                    </div>
                    {this.state.like ?
                      <div className="item-footer-button-col">
                        <span><i className="fa fa-heart active"></i></span>
                      </div>
                      :
                      <div className="item-footer-button-col" onClick={() => addLike(postContent.id)}>
                        <span><i className="fa fa-heart"></i></span>
                      </div>}
                    {this.state.going ?
                      <div className="item-footer-button-col active">
                        <span><i className="fa fa-check"></i></span>
                        <span>I am going</span>
                      </div>
                      :
                      <div className="item-footer-button-col" onClick={() => addGoing(postContent.id)} >
                        <span><i className="fa fa-check"></i></span>
                        <span>Join</span>
                      </div>}
                  </div> :
                  <div className="item-footer-button-section" onClick={() => this.setState({ new_comment: '' })}>
                    <div className="item-footer-decline">
                      <span><i className="fa fa-times"></i></span>
                    </div>
                    <div className="item-footer-comment">
                      <input placeholder="Input your comment here" name="new_comment" value={this.state.new_comment} onChange={this.handleChange} />
                    </div>
                    <div className="item-footer-submit" onClick={this.postComment}>
                      <span><i className="fa fa-paper-plane" ></i></span>
                    </div>
                  </div>
              }
            </div>
          </div>
        </div>
        : <div className="container"><h1>Looks like you haven't selected a user</h1></div>
    )
  }
}
const mapStateToProps = (state) => ({
  postContent: state.postReducer.currentPost,
  data: state.postContentReducer,
})

const mapDispatchToProps = (dispatch) => {
  return {
    postComment: (comment, postId) => {
      dispatch(postCommentReload(comment, postId))
    },
    getComments: (post_id) => {
      dispatch(getComments(post_id))
    },
    addGoing: (post_id) => {
      dispatch(postGoingReload(post_id))
    },
    getGoing: (post_id) => {
      dispatch(getGoing(post_id))
    },
    addLike: (post_id) => {
      dispatch(addLikeReload(post_id))
    },
    getLikes: (post_id) => {
      dispatch(getLikes(post_id))
    },
  }
}

const PostContentContainer = connect(
  mapStateToProps, mapDispatchToProps
)(PostContent)

export default PostContentContainer;
