import React from "react";
import PostItem from "./components/postItem";
import Waypoint from "react-waypoint";
import { connect } from "react-redux";
import {
  fetchPosts,
  setPost,
  searchPosts,
  clearSearch,
  getComments,
  getGoing,
  getLikes
} from "./action";
import { categories } from "./constants/categories";
import Loading from "../../components/loading";
import Header from "../../components/header";
import LogOut from "../../components/logout";
import "./posts.css";
class Posts extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      openSearch: false,
      page: 1
    };
  }

  componentWillReceiveProps(nextProps) {
    const { data } = nextProps;
    if (data.is_search) {
      this.setState({ openSearch: false });
    } else {
      if (!data.loading) {
        this.setState({ page: this.state.page + 1 });
      }
    }
  }

  selectPost(post) {
    const { setPost, getComments, getGoing, getLikes } = this.props;
    setPost(post);
    getComments(post.id);
    getGoing(post.id);
    getLikes(post.id);
  }

  render() {
    const { data, fetchPosts, setPost, search, clearSearch } = this.props;
    return (
      <div className="container">
        <Header openSearch={this.state.openSearch}>
          <span
            className="left-icon"
            onClick={() =>
              this.setState({ openSearch: !this.state.openSearch })
            }
          >
            <i className="fa fa-search" />
          </span>
          {this.state.openSearch ? (
            <span className="searchOption fadeInLeft">
              {categories.map((cat, i) => {
                return (
                  <span key={i} onClick={() => search(cat)}>
                    {cat}
                  </span>
                );
              })}
            </span>
          ) : (
            <span>
              <span className="center-icon">
                <img src="http://www.freepngimg.com/download/superman_logo/10-2-superman-logo-png-hd.png" />
              </span>
              <LogOut />
            </span>
          )}
        </Header>
        {data.loading ? <Loading /> : ""}
        <div>
          {!data.is_search ? (
            <div id={"posts"}>
              {data.posts.map((post, i) => {
                return (
                  <PostItem
                    key={i}
                    post={post}
                    onClick={this.selectPost.bind(this, post)}
                  />
                );
              })}
              {!data.empty && !data.loading ? (
                <Waypoint onEnter={() => fetchPosts(this.state.page)} />
              ) : (
                ""
              )}
            </div>
          ) : (
            <div id={"search_data"}>
              <div className="search_result">
                <span className="left-icon">
                  <h4>{data.searchPosts.length} Results</h4>
                </span>
                <div className="clear_search">
                  <span
                    className="clear_search_span right-icon"
                    onClick={() => clearSearch()}
                  >
                    Clear Search
                  </span>
                </div>
                <div className="channel_name">
                  <h5>Searched for Channel {data.category}</h5>
                </div>
              </div>
              {data.searchPosts.map((post, i) => {
                return (
                  <PostItem
                    key={i}
                    post={post}
                    onClick={this.selectPost.bind(this, post)}
                  />
                );
              })}
            </div>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  data: state.postReducer
});

const mapDispatchToProps = dispatch => {
  return {
    fetchPosts: page => {
      dispatch(fetchPosts(page));
    },
    setPost: user => {
      dispatch(setPost(user));
    },
    search: category => {
      dispatch(searchPosts(category));
    },
    clearSearch: () => {
      dispatch(clearSearch());
    },
    getComments: post_id => {
      dispatch(getComments(post_id));
    },
    getGoing: post_id => {
      dispatch(getGoing(post_id));
    },
    getLikes: post_id => {
      dispatch(getLikes(post_id));
    }
  };
};

const PostsContainer = connect(mapStateToProps, mapDispatchToProps)(Posts);

export default PostsContainer;
