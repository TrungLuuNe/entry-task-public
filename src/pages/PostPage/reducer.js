const initalState = {
  posts: [],
  loading: false,
  currentPost: null,
  error: null,
  empty: false,
  searchPosts: [],
  category: null,
  is_search: false,
};


// REDCUER
function postsReducer(state = initalState, action) {
  let posts, empty, searchPosts;
  switch (action.type) {
    case 'FETCH_POST_PENDING':
      return { ...state, loading: true };
    case 'FETCH_POST_FULFILLED':
      posts = action.payload.data;
      empty = posts.length ? false : true
      posts = [...state.posts, ...posts]
      return { ...state, loading: false, posts, empty };
    case 'FETCH_POST_REJECTED':
      return { ...state, loading: false, error: `${action.payload.message}` };
    case 'SET_POST':
      return {...state, currentPost: action.payload};
    case 'SEARCH_POST_PENDING':
      return { ...state, loading: true, is_search: true, searchPosts: [] };
    case 'SEARCH_POST_FULFILLED':
    searchPosts = action.payload.data;
      return { ...state, loading: false, searchPosts, category: action.meta, is_search: true };
    case 'SEARCH_POST_REJECTED':
      return { ...state, loading: false, error: `${action.payload.message}`, is_search: false };

    case 'SEARCH_CLEAR':
      return { ...state, loading: false, is_search: false, searchPosts: [] };
    default:
      return state;
  }
}

export default postsReducer;
