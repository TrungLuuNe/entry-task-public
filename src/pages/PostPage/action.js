import axios from 'axios';
export function getComments(post_id) {
    return {
        type: 'FETCH_COMMENTS',
        payload: axios.get('http://localhost:3000/comments?postId=' + post_id)
    };
}

export function getGoing(post_id) {
    return {
        type: 'FETCH_GOING',
        payload: axios.get('http://localhost:3000/going?postId=' + post_id)
    };
}

export function getLikes(post_id) {
    return {
        type: 'FETCH_LIKES',
        payload: axios.get('http://localhost:3000/likes?postId=' + post_id)
    };
}

export function fetchPosts(page) {
    return {
        type: 'FETCH_POST',
        payload: axios.get('http://localhost:3000/posts2?_page=' + page)
    };
}

export function setPost(user) {
    return {
        type: 'SET_POST',
        payload: user
    };
}

export function searchPosts(category) {
    return {
        type: 'SEARCH_POST',
        meta: category,
        payload: axios.get('http://localhost:3000/posts2?category=' + category)
    };
}

export function clearSearch() {
    return {
        type: 'SEARCH_CLEAR',
    };
}