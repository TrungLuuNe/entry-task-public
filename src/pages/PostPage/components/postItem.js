import React from 'react';
import { Link } from 'react-router-dom';

import './postItem.css';

const PostItem = (props) => (
  <Link
    className='box'
    onClick={props.onClick}
    to={`/post/`}
  >
    <div className="item">
      <div className="item-header">
        <span className="item-header-avatar left-icon"><img src={props.post.picture.thumbnail} /></span>
        <span className="item-header-username left-icon">{props.post.login.username}</span>
        <span className="item-header-channel right-icon">{props.post.category}</span>
      </div>

      <div className="item-content">
        <p className="item-content-title">Activity Title Name Make it Longer May Longer than One Line</p>
        <div>
          <span><i className="item-content-clock item-content-icon fa fa-clock-o"></i></span><span className="item-content-time">14 May 2016 12:22 - 14 May 2016 18:00</span>
        </div>

        <p className="item-content-text">[No longer than 300 chars] Vivamus sagittis, diam in lobortis, sapien arcu mattis erat, vel aliquet sem urna et risus. Ut feugiat sapien mi potenti...</p>
        <span><i className="item-content-check-green item-content-icon fa fa-check"></i></span>
        <span className="item-content-check-text item-content-footer-text-normal">I am going!</span>
        <span><i className="item-content-heart-red item-content-icon fa fa-heart"></i></span>
        <span className="item-content-heart-text item-content-footer-text-normal">I like it</span>
      </div>
    </div>
  </Link>
)

PostItem.propTypes = {
  post: React.PropTypes.object.isRequired,
};

export default PostItem