import { render } from "react-dom";
import React from "react";
import Posts from "./pages/PostPage/posts";
import Login from "./pages/LoginPage/login";
import Loading from "./components/loading"
import { Provider } from "react-redux";
import UsersStore from "./store";
import PostContent from "./pages/PostContentPage/postContent";
import { BrowserRouter, Route } from "react-router-dom";
import PrivateRoute from "./components/private_route";
import App from "./components/app";

render(
  <Provider store={UsersStore}>
    {/* <PersistGate loading={<Loading></Loading>} persistor={UsersStore().persistor}> */}
      <App />
    {/* </PersistGate> */}
  </Provider>,
  document.getElementById("app")
);
